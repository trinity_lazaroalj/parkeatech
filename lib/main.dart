import 'package:flutter/material.dart';
import 'package:parkeatech/views/loginview.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: '/',
      onGenerateRoute: RouteGenerator.generateRoute,
    );
  }
}

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final argument = settings.arguments;
    switch (settings.name) {
      case '/':
      return MaterialPageRoute(
        builder: (BuildContext context){ return new LoginView(); }
      );
      // case 'studentview':
      // if (argument is Student) {
      //   return MaterialPageRoute(
      //     builder: (BuildContext context){ return new Student(argument); }
      //   );
      // }
      // case 'guardview':
      // if (argument is Guard) {
      //   return MaterialPageRoute(
      //     builder: (BuildContext context){ return new Guard(argument); }
      //   )
      // }
    }
  }
}